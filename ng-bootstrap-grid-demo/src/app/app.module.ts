import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { Accordion } from './app.accordion-toggle';
import { Grid } from './app.grid';

@NgModule({
  declarations: [
    Accordion,
    Grid
  ],
  imports: [
    BrowserModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [Accordion, Grid]
})
export class AppModule { }
